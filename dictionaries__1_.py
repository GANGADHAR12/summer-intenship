my_dict = {'name': 'Gangadhar', 'age': 20, 'city': 'Hyderabad'}
print(len(my_dict))
print(my_dict.keys())
print(my_dict.values())
print(my_dict.items()) 
print(my_dict.get('name'))
print(my_dict.get('occupation', 'N/A'))
print(my_dict.get('age'))
print(my_dict.get('city'))
